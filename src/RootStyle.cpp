#include "RootStyle.hh"

#include <TAxis.h>
#include <TCanvas.h>
#include <TEfficiency.h>
#include <TGraph.h>
#include <TGraphAsymmErrors.h>
#include <TGraphErrors.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TMath.h>
#include <TPaletteAxis.h>
#include <TPaveText.h>
#include <TProfile.h>
#include <TString.h>
#include <TStyle.h>
#include <TLatex.h>

#include <iostream>

namespace RootStyle{

  Int_t _DefaultFont = 133;

  int PDFFile::fileNumber = 0;
  int PSFile::fileNumber = 0;

  void SetStyle(Int_t DefaultFont){
    _DefaultFont = DefaultFont;
    gROOT->SetStyle("Plain");
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(0);
    gStyle->SetPalette(1);
    gStyle->SetLabelFont(DefaultFont,"XYZ");
    gStyle->SetLabelFont(DefaultFont,"other");
    gStyle->SetTitleFont(DefaultFont,"XYZ");
    gStyle->SetTitleFont(DefaultFont,"other");
    gStyle->SetTitleOffset(1.2,"X");
    gStyle->SetTitleOffset(1.6,"Y");
    gStyle->SetLabelSize(0.05,"XZY");
    gStyle->SetTitleSize(0.05,"XZY");
    // gStyle->SetPaperSize(kA4);
    gROOT->ForceStyle();
  }

  void SetBeamerStyle(Int_t DefaultFont){
    _DefaultFont = DefaultFont;
    gROOT->SetStyle("Plain");
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(0);
    gStyle->SetPalette(1);
    gStyle->SetLabelFont(DefaultFont,"XYZ");
    gStyle->SetLabelFont(DefaultFont,"other");
    gStyle->SetTitleFont(DefaultFont,"XYZ");
    gStyle->SetTitleFont(DefaultFont,"other");
    gStyle->SetTitleOffset(0.9,"X");
    gStyle->SetTitleOffset(1.2,"Y");
    gStyle->SetLabelSize(0.05,"XZY");
    gStyle->SetTitleSize(0.05,"XZY");
    // gStyle->SetPaperSize(kA4);
    gROOT->ForceStyle();
  }

  void SetCDRStyle(Int_t DefaultFont){
    _DefaultFont = DefaultFont;
    gROOT->SetStyle("Plain");// Default white background for all plots

    // set bkg color of all to 10: white, but not 0
    gStyle->SetCanvasColor(10);
    gStyle->SetFrameFillColor(10);
    gStyle->SetStatColor(10);
    gStyle->SetPadColor(10);
    gStyle->SetFillColor(10);
    // SetPaperSize wants width & height in cm: A4 is 20,26 & US is 20,24
    gStyle->SetPaperSize(20,26);
    // No yellow border around histogram
    gStyle->SetDrawBorder(0);
    // removve border of canvas
    gStyle->SetCanvasBorderMode(0);
    // remove border of pads
    gStyle->SetPadBorderMode(0);
    // default text size
    gStyle->SetTextSize(0.05);
    gStyle->SetTitleSize(0.06,"xyz");
    gStyle->SetLabelSize(0.04,"xyz");
    // title offset: distance between given text and axis, here x,y,z
    gStyle->SetLabelOffset(0.01,"x");//AS Customized //Default 0.01
    gStyle->SetLabelOffset(0.01,"yz");//AS Customized
    gStyle->SetTitleOffset(0.9,"yz");
    gStyle->SetTitleOffset(0.7,"x");
    // Use visible font for all text
    gStyle->SetTitleFont(DefaultFont);
    gStyle->SetTitleFontSize(0.06);
    gStyle->SetStatFont(DefaultFont);
    gStyle->SetTextFont(DefaultFont);
    gStyle->SetLabelFont(DefaultFont,"xyz");
    gStyle->SetTitleFont(DefaultFont,"xyz");
    // big marker points
    gStyle->SetMarkerSize(1.2);
    //set palette in 2d histogram to nice and colorful one
    //gStyle->SetPalette(1,0); 
    gStyle->SetPalette(kBird); //-> use ROOT6 kBird default instead of kRainBow for increased plot readability
    //No Title for histograms
    gStyle->SetOptTitle(0);
    // show the errors on the stat box
    gStyle->SetOptStat(0); //AS CHANGED
    // show errors on fitted parameters
    gStyle->SetOptFit(0); //AS CHANGED
    // number of decimals used for errors
    gStyle->SetEndErrorSize(5);

    // set line width to 2 by default so that histograms are visible when printed small
    // idea: emphasize the data, not the frame around
    gStyle->SetHistLineWidth(3);// is much better visible for multiple lines and faint colors!, Changed from 2

    //AS Customized
    gStyle->SetLabelOffset(0.01,"other");//AS Customized
    gStyle->SetTitleOffset(1.9,"other");
    gStyle->SetLabelFont(DefaultFont,"other");
    gStyle->SetTitleFont(DefaultFont,"other");

    //These are the Default canvas sizes
    gStyle->SetCanvasDefW(800);
    gStyle->SetCanvasDefH(700);

    gROOT->ForceStyle();
  }


  void SetCDRStyle2(Int_t DefaultFont){
    _DefaultFont = DefaultFont;


    gROOT->SetStyle("Plain"); /*Default white background for all plots*/
     
    /* set bkg color of all to 10: white, but not 0*/

    gStyle->SetCanvasColor(10);
    gStyle->SetFrameFillColor(10);
    gStyle->SetStatColor(10);
    gStyle->SetPadColor(10);

    gStyle->SetFillColor(10);

    gStyle->SetTitleFillColor(0);

    /* SetPaperSize wants width & height in cm: A4 is 20,26 & US is 20,24*/
    gStyle->SetPaperSize(20, 26); 
    /* No yellow border around histogram*/
    gStyle->SetDrawBorder(0);
    /* remove border of canvas*/
    gStyle->SetCanvasBorderMode(0);
    /* remove border of pads*/
    gStyle->SetPadBorderMode(0);
    gStyle->SetFrameBorderMode(0);
    gStyle->SetLegendBorderSize(0);//AS:: Only change
  
    /* default text size*/
    gStyle->SetTextSize(0.05);
    gStyle->SetTitleSize(0.06,"xyz"); //AS changed from 0.07 for now, to be consistent with other plots!
    //    gStyle->SetTitleSize(0.07,"xyz"); //AS changed from 0.07 for now, to be consistent with other plots!
    gStyle->SetLabelSize(0.06,"xyz");

    /* title offset: distance between given text and axis, here x,y,z*/

    gStyle->SetLabelOffset(0.015,"xyz");
    gStyle->SetTitleOffset(1.1,"yz");
    gStyle->SetTitleOffset(1.0,"x");

    //AS AS AS
    gStyle->SetLabelOffset(0.01,"x");//AS Customized //Default 0.015
    gStyle->SetLabelOffset(0.01,"yz");//AS Customized
    gStyle->SetLabelOffset(0.005,"y");//AS Customized


    // Use visible font for all text
    gStyle->SetTitleFont(DefaultFont);
    gStyle->SetTitleFontSize(0.06);
    gStyle->SetStatFont(DefaultFont);
    gStyle->SetStatFontSize(0.07);
    gStyle->SetTextFont(DefaultFont);
    gStyle->SetLabelFont(DefaultFont,"xyz");
    gStyle->SetTitleFont(DefaultFont,"xyz");
    gStyle->SetTitleBorderSize(0);
    gStyle->SetStatBorderSize(1);

    /* big marker points*/
    gStyle->SetMarkerStyle(1);
    gStyle->SetLineWidth(2);  
    gStyle->SetMarkerSize(1.2);
    /*set palette in 2d histogram to nice and colorful one*/
    //gStyle->SetPalette(1,0); 
    gStyle->SetPalette(kBird); //-> use ROOT6 kBird default instead of kRainBow for increased plot readability

    /*No title for histograms*/
    gStyle->SetOptTitle(0);
    /* show the errors on the stat box */
    gStyle->SetOptStat(0); 
    /* show errors on fitted parameters*/
    gStyle->SetOptFit(0); 
    /* number of decimals used for errors*/
    gStyle->SetEndErrorSize(5);   

    /* set line width to 2 by default so that histograms are visible when printed small
       idea: emphasize the data, not the frame around*/
    gStyle->SetHistLineWidth(2);
    gStyle->SetFrameLineWidth(2);
    gStyle->SetFuncWidth(2);
    gStyle->SetHistLineColor(kBlack);
    gStyle->SetFuncColor(kRed);
    gStyle->SetLabelColor(kBlack,"xyz");


    //set the margins
    gStyle->SetPadBottomMargin(0.18);
    gStyle->SetPadTopMargin(0.08);
    gStyle->SetPadRightMargin(0.08);
    gStyle->SetPadLeftMargin(0.17);

    //set the number of divisions to show
    gStyle->SetNdivisions(506, "xy");

    //turn off xy grids
    gStyle->SetPadGridX(0);
    gStyle->SetPadGridY(0);

    //set the tick mark style
    //    gStyle->SetPadTickX(1);
    gStyle->SetPadTickY(1);
    gStyle->SetPadTickX(1);

    gStyle->SetCanvasDefW(800);
    gStyle->SetCanvasDefH(700);

    gROOT->ForceStyle();
  }



  void SetPad(TCanvas *c2) {
    c2->GetPad(0)->SetLeftMargin(0.17);
    c2->GetPad(0)->SetRightMargin(0.08);
  }
  void SetPad1D(TCanvas *c2) {    SetPad(c2);  }


  void SetPad2D(TPad *pad) {
    pad->SetLeftMargin(0.15);
    pad->SetRightMargin(0.15);
    pad->SetTopMargin(0.15);
    pad->SetBottomMargin(0.15);
  }

  void SetPad2D(TCanvas *c2) {    SetPad2D((TPad*)c2->GetPad(0));  }



  void BinLogX(TH1*h, TString Saxis){
    TAxis *axis = 0;
    if(Saxis == "X") {
      axis = h->GetXaxis();
    } else if(Saxis == "Y") {
      axis = h->GetYaxis();
    } else {
      return;
    }
    int bins = axis->GetNbins();
    Axis_t from = axis->GetXmin();
    Axis_t to = axis->GetXmax();
    Axis_t width = (to - from) / bins;
    //    Axis_t *new_bins = new Axis_t[bins + 1];
    std::vector<Axis_t> new_bins(bins + 1);
    for (int i = 0; i <= bins; ++i) {
      new_bins[i] = TMath::Power(10, from + i * width);
      if(new_bins[i] > 1e15) {
	std::cout << "The new bin range is huge, better check what you are doing! "<< new_bins[i] << std::endl;
      }
      // std::cout << i << "  " << new_bins[i] << std::endl;
    }
    axis->Set(bins, &(new_bins[0]));
    // std::cout << "New XMin" <<    axis->GetXmin() << std::endl;
    //    delete new_bins;
  }

  void NormalizeLogHisto(TH1 *h){
    //    const Double_t tot_integral = h->Integral();
    for (Int_t bin = 1; bin <= h->GetXaxis()->GetNbins(); ++bin) {
      if (h->GetBinWidth(bin) > 0.) {
	// h->SetBinContent(bin,h->GetBinContent(bin)/h->GetBinWidth(bin)/tot_integral);
	// h->SetBinError(bin,h->GetBinError(bin)/h->GetBinWidth(bin)/tot_integral);
	h->SetBinContent(bin,h->GetBinContent(bin)/h->GetBinWidth(bin));
	h->SetBinError(bin,h->GetBinError(bin)/h->GetBinWidth(bin));
      } else
	std::cout <<"something is wrong: binwidth 0 for bin " << bin << std::endl;
    }
  }//NormalizeLogHisto

  void NormalizeLogHisto(TH2 *h) {
    //    h->Scale(1./h->Integral());
    const Int_t binsx = h->GetNbinsX();
    const Int_t binsy = h->GetNbinsY();
    Int_t binx, biny, binz;
    for (Int_t bin = 1; bin <= (binsx*binsy); ++bin) {
      h->GetBinXYZ(bin, binx, biny, binz);
      Double_t widthx=h->GetXaxis()->GetBinWidth(binx);
      Double_t widthy=h->GetYaxis()->GetBinWidth(biny);
      //h->GetXaxis()->GetBinUpEdge(binx) - h->GetXaxis()->GetBinLowEdge(binx);
      //Double_t widthy=h->GetYaxis()->GetBinUpEdge(biny) - h->GetYaxis()->GetBinLowEdge(biny);
      if ( (widthx*widthy) > 0.) {
  	h->SetBinContent(bin,h->GetBinContent(bin)/widthx/widthy);
  	h->SetBinError(bin,h->GetBinError(bin)/widthx/widthy);
      } else
  	std::cout <<"something is wrong: binwidth 0 for bin " << bin <<std::endl;
    }
  }//NormalizeLogHisto TH2


  void SaveCanvas(TCanvas *canvas, TString filename) {
    canvas->SaveAs(filename+".eps");
    canvas->SaveAs(filename+".pdf");
    canvas->SaveAs(filename+"Macro.C");
    canvas->SaveAs(filename+".root");
  }

  void SetAllColors( TH1* object, Color_t color ){
    object->SetLineColor(color);
    object->SetMarkerColor(color);
  }

  void SetAllColors( TEfficiency* object, Color_t color ){
    object->Paint("");
    //SetAllColors(object->GetPaintedGraph(), color);
    object->GetPaintedGraph()->SetLineColor(color);
    object->GetPaintedGraph()->SetMarkerColor(color);
    object->SetLineColor(color);
    object->SetMarkerColor(color);
    object->SetFillColor(kWhite);
    object->SetLineWidth(3);
  }


  void SetAllColors( TGraph* object, Color_t color ){
    object->SetLineColor(color);
    object->SetMarkerColor(color);
    object->SetFillColor(kWhite);
  }

  void SetAllColors( TGraphErrors* object, Color_t color ){
    object->SetLineColor(color);
    object->SetMarkerColor(color);
    object->SetFillColor(kWhite);
  }


  TPaletteAxis* MovePaletteHorizontally (TH1 *histo, Double_t step) {
    TPaletteAxis *palette = (TPaletteAxis*)histo->GetListOfFunctions()->FindObject("palette");
    if (palette) {
      palette->SetX1NDC(palette->GetX1NDC()-step);
      palette->SetX2NDC(palette->GetX2NDC()-step);
      palette->GetAxis()->SetLabelOffset(0.01);
    } else {
      std::cout << "Palette not found!"  << std::endl;
    }
    return palette;
  }



  //  myRoot.BuildLegend(c1, 0.7,0.8,0.9,0.9);
  TLegend* BuildLegend( TCanvas& canvas, Double_t x1, Double_t y1, Double_t x2, Double_t y2) { 
    return BuildLegend( &canvas, x1, y1, x2, y2); 
  }

  TLegend* BuildLegend( TCanvas& canvas, Double_t x1, Double_t y1, Double_t x2, Double_t y2, TString& Header) { 
    return BuildLegend( &canvas, x1, y1, x2, y2, Header); 
  }


  TLegend* BuildLegend(TCanvas *canvas, Double_t x1, Double_t y1, Double_t x2, Double_t y2) {
    TLegend *leg = (TLegend*)canvas->BuildLegend(x1, y1, x2, y2);
    if(leg) {
      leg->SetTextFont(_DefaultFont);
      leg->SetFillColor(kWhite);
      leg->SetTextSize(0.04);
      leg->SetMargin(0.15);
    }
    return leg;
  }

  TLegend* BuildLegend(TCanvas *canvas, Double_t x1, Double_t y1, Double_t x2, Double_t y2, TString& Header) {
    TLegend *leg = (TLegend*)canvas->BuildLegend(x1, y1, x2, y2, Header);
    if(leg) {
      leg->SetTextFont(_DefaultFont);
      leg->SetFillColor(kWhite);
      leg->SetTextSize(0.06);
      leg->SetMargin(0.15);
    }
    return leg;
  }

  TLegend* BuildLegend(TCanvas& canvas, Double_t x1, Double_t y2, Double_t sizePerEntry,
                       std::vector<std::string> const& lines, int at){
    TLegend* leg = BuildLegend(canvas, x1, y2-0.1, x1+0.3, y2);
    AddLegendHeader(leg, lines, at);
    RightSizeLegend(leg, sizePerEntry);
    return leg;
  }


  void AddLegendHeader(TLegend* leg, std::vector<std::string> const& lines, int at){
    int counter (at);
    for (std::vector<std::string>::const_iterator lineIt =  lines.begin(); lineIt != lines.end(); ++lineIt) {
      TLegendEntry* newEntry = new TLegendEntry((TObject*)NULL, (*lineIt).c_str(), "h");
      leg->GetListOfPrimitives()->AddBefore(leg->GetListOfPrimitives()->At(counter), newEntry );
      ++counter;
    }
  }


  TPaveText* CreateTextBox(Double_t x1, Double_t y1, std::vector<std::string> const& lines){

    TPaveText* tpt= new TPaveText(x1, y1, x1+0.3, y1-lines.size() * GetLegendEntrySize(), "brNDC");
    for (auto& line: lines) {
      tpt->AddText( line.c_str() );
    }
    tpt->SetTextSize( GetLegendTextSize() );
    tpt->SetBorderSize(0);
    tpt->SetTextAlign(12);//Left adjusted (10) and vertically centre aligned (2) see TAttText Doc
    //tpt->SetY1( tpt->GetY2() - lines.size() * GetLegendEntrySize() );
    return tpt;
  }


  void RightSizeLegend(TLegend* leg, double sizePerLine) {
    leg->SetY1( leg->GetY2() - sizePerLine * leg->GetListOfPrimitives()->GetSize() );
  }


  TLegend* Build2DLegend(TCanvas *canvas) {
    // TLegend *leg = BuildLegend(canvas, 0.4, 0.90, 0.98, 0.95);
    // if(leg) {
    //   leg->SetTextFont(132);
    //   leg->SetFillColor(kWhite);
    //   leg->SetTextSize(0.04);
    // }
    // return leg;
    return BuildLegend(canvas, 0.4, 0.90, 0.98, 0.95);
  }

  void SetPaveCoordinates(TPave *pave, double x1, double y1, double x2, double y2) {
    pave->SetX1NDC(x1);
    pave->SetX2NDC(x2);
    pave->SetY1NDC(y1);
    pave->SetY2NDC(y2);
  }


  TProfile* AverageHistograms(std::vector<TH1D*> histoVector) {
    if (histoVector.size() < 2) { return NULL;}
    Int_t nbins = histoVector.front()->GetNbinsX();
    Int_t histoNumber = 0;
    Double_t *edges = new Double_t[nbins+1];
    histoVector.front()->GetLowEdge(edges);
    edges[nbins] = histoVector.front()->GetXaxis()->GetXmax();
    TProfile *profile = new TProfile(histoVector.front()->GetName()+TString("Profile"),histoVector.front()->GetTitle()+TString("Profile"),
				     nbins,edges,
				     0, 100.*histoVector.front()->GetMaximum());
    for(std::vector<TH1D*>::iterator iter = histoVector.begin(); iter < histoVector.end(); ++iter){
      for(Int_t i = 1; i <= nbins; ++i){
	profile->Fill((*iter)->GetBinCenter(i),(*iter)->GetBinContent(i));
      }
      ++histoNumber;
    }
    profile->SetXTitle(histoVector.front()->GetXaxis()->GetTitle());
    profile->SetYTitle(histoVector.front()->GetYaxis()->GetTitle());
    return profile;
  }//AverageHistograms



  void SetAxisOfTEfficiency(TEfficiency &tEff, std::string const& TitleX, std::string const& TitleY) {
    TGraphAsymmErrors* tgae = tEff.GetPaintedGraph();
    if(tgae) {
      // tEff->GetPaintedGraph()->SetMaximum(1.35);
      tEff.GetPaintedGraph()->GetXaxis()->SetTitle(TitleX.c_str());
      tEff.GetPaintedGraph()->GetYaxis()->SetTitle(TitleY.c_str());
    } else {
      std::cout << "Graph of " << tEff.GetName() << " not found, Repainting"  << std::endl;
      tEff.Paint("");
      SetAxisOfTEfficiency(tEff, TitleX, TitleY);
    }
  }//SetAxisOfTEfficiency


  double AutoSetYRange( TCanvas& canv, double maxScale ) {
    TList* list = canv.GetListOfPrimitives();
    double maximum=0;
    int firstHist = -1;
    //    int isCanvasLogY = canv.GetLogy();
    for( int iPrims = 0; iPrims <= list->LastIndex(); ++iPrims ) {
      TH1* hist = dynamic_cast<TH1*>( list->At(iPrims) );
      if( hist ) {
	//Remember histo to set maximum of, which is the first one drawn
	if( firstHist == -1 ) { firstHist = iPrims; }
	if( hist->GetMaximum() > maximum ) { maximum = hist->GetMaximum(); }
      }
    }

    if( firstHist != -1 ) {
      dynamic_cast<TH1*>( list->At(firstHist) )->SetMaximum(maximum*maxScale);
      return maximum*maxScale;
    } else {
      std::cout << __func__ << " No Histograms found"  << std::endl;
      return -1;
    }
    
  }

  ///Make the peak for all histograms be the same
  double EqualiseHistogramPeak( TCanvas& canv ) {
    TList* list = canv.GetListOfPrimitives();
    double maximum=0;
    //    int isCanvasLogY = canv.GetLogy();
    for( int iPrims = 0; iPrims <= list->LastIndex(); ++iPrims ) {
      TH1* hist = dynamic_cast<TH1*>( list->At(iPrims) );
      if( hist ) {
	if( hist->GetMaximum() > maximum ) { maximum = hist->GetMaximum(); }
      }
    }

    for( int iPrims = 0; iPrims <= list->LastIndex(); ++iPrims ) {
      TH1* hist = dynamic_cast<TH1*>( list->At(iPrims) );
      if( hist ) {
	const double localMaximum = hist->GetMaximum();
	hist->Scale(maximum/localMaximum);
      }

    }//for all histograms
    return 0;
  }


  void AutoScaleXaxis( THStack *s ) {

    double min = 0, max = 0;

    TH1  *hist  = 0;
    TList *hists = s->GetHists();

    for( int iHist = 0; iHist <= hists->LastIndex(); ++iHist ) {
      int bin=0;
      hist = dynamic_cast<TH1*>( hists->At( iHist ) );

      // max x
      bin = hist->FindLastBinAbove(0, 1);
      if( bin != -1 && hist->GetBinLowEdge( bin + 1 ) > max ) {
	max = hist->GetBinLowEdge( bin + 1 );
      }

      // min x
      bin = hist->FindFirstBinAbove(0, 1);
      if( bin != -1 && hist->GetBinLowEdge( bin - 1 ) < min ) {
	min = hist->GetBinLowEdge( bin - 1 );
      }

    }
    s->GetXaxis()->SetRangeUser( min, max );
  }//AutoScaleXaxis


  void AutoScaleXaxis( TH1 *hist ) {
    double min = 0, max = 0;
    //int bin=0;
    // max x
    max = hist->FindLastBinAbove(0, 1);
    // min x
    min = hist->FindFirstBinAbove(0, 1);
    hist->GetXaxis()->SetRangeUser( min, max );
  }//AutoScaleXaxis




  //////////////////////////////////////////////////////////////////////
  //PDFFile CLASS started
  //////////////////////////////////////////////////////////////////////
  PDFFile::~PDFFile() {
    if(fileOpened) {
      PDFFile::CloseFile();
    }
  }

  PDFFile::PDFFile(): fileOpened(false) {
    ++fileNumber;
    fileName = Form("myPDFFile_%i.pdf",fileNumber);
  }

  PDFFile::PDFFile(TString filename):  fileOpened(false) {
    fileName = filename;
    ++fileNumber;
  }

  void PDFFile::AddCanvas(TCanvas *canv, Option_t* title){
    TString myTitle(title);
    myTitle.ReplaceAll(".root","");
    if(!fileOpened){
      if( strlen(title) ) {
	canv->Print(fileName+TString("("),"pdf,Title:"+myTitle);
      } else {
	std::cout << "No Title"  << std::endl;
	canv->Print(fileName+TString("("),"pdf");
      }
      fileOpened = true;
    } else {
      if( strlen(title) ) {
	canv->Print(fileName,"pdf,Title:"+myTitle);
      } else {
	canv->Print(fileName,"pdf");
      }
    }
  }//PDFFile::AddCanvas

  void PDFFile::CloseFile(){
    TCanvas c2("emptyPDFCanv","arg",100, 100);
    c2.cd();
    c2.Print(fileName+TString("]"),"pdf");
    fileOpened = false;
  }
  //////////////////////////////////////////////////////////////////////
  //PDFFile CLASS finished
  //////////////////////////////////////////////////////////////////////



  //////////////////////////////////////////////////////////////////////
  //PSFile CLASS started
  //////////////////////////////////////////////////////////////////////
  PSFile::~PSFile() {
    if(fileOpened) {
      PSFile::CloseFile();
    }

  }

  PSFile::PSFile(): fileOpened(false) {
    ++fileNumber;
    fileName = Form("myPSFile_%i.ps",fileNumber);
  }

  PSFile::PSFile(TString filename):  fileOpened(false) {
    fileName = filename;
    ++fileNumber;
  }

  void PSFile::AddCanvas(TCanvas *canv){
    if(!fileOpened){
      canv->Print(fileName+TString("("),"ps");
      fileOpened = true;
    } else {
      canv->Print(fileName,"ps");
    }
  }

  void PSFile::CloseFile(){
    TCanvas c2("emptyPSCanv","arg",100, 100);
    c2.cd();
    c2.Print(fileName+TString("]"),"ps");
    fileOpened = false;
  }

  //////////////////////////////////////////////////////////////////////
  //PSFile CLASS finished
  //////////////////////////////////////////////////////////////////////

  MyMarker::MyMarker() {
    Markers = new std::vector<Marker_t>;
  }
  MyMarker::~MyMarker() {
    delete Markers;
  }

  void  MyMarker::SetMarkers() {
    Markers->push_back(kDot		 );
    Markers->push_back(kPlus		 );
    Markers->push_back(kStar		 );
    Markers->push_back(kCircle		 );
    Markers->push_back(kOpenCircle	 );
    Markers->push_back(kOpenSquare	 );
    Markers->push_back(kOpenTriangleUp	 );
    Markers->push_back(kOpenDiamond	 );
    Markers->push_back(kOpenCross	 );
    Markers->push_back(kOpenStar	 );
    Markers->push_back(kMultiply	 );
    Markers->push_back(kFullDotSmall	 );
    Markers->push_back(kFullDotMedium	 );
    Markers->push_back(kFullDotLarge	 );
    Markers->push_back(kFullTriangleDown );
    //    Markers->push_back(kFullCross        );
    Markers->push_back(kFullStar         );
    Markers->push_back(kFullTriangleUp	 );
    Markers->push_back(kFullCircle	 );
    Markers->push_back(kFullSquare	 );
  }


  Marker_t MyMarker::GetMarker() {
    Marker_t myMarker = kDot;
    if(Markers->size() > 0 ) {
      myMarker = Markers->back();
      Markers->pop_back();
      return myMarker;
    } else {
      SetMarkers();
      return GetMarker();
    }
  }

  MyColor::MyColor() {
    Colors = new std::vector<Color_t>;
  }
  MyColor::~MyColor() {
    delete Colors;
  }

  void MyColor::SetColors() {
    Colors->push_back(kRed-10);
    Colors->push_back(kViolet+2);
    Colors->push_back(kOrange);
    Colors->push_back(kYellow+4);
    Colors->push_back(kPink+6);
    Colors->push_back(kRed+2);
    Colors->push_back(kBlue);
    Colors->push_back(kCyan+1);
    Colors->push_back(kRed-7);
    Colors->push_back(kGreen+2);
    Colors->push_back(kBlack);
  }

  Color_t MyColor::GetColor() {
    Color_t myColor = kBlack;
    if(Colors->size() > 0 ) {
      myColor = Colors->back();
      Colors->pop_back();
      return myColor;
    } else {
      SetColors();
      return GetColor();
    }
  }


void SetCDRStyleReal() {
  _DefaultFont = 42;

   gROOT->SetStyle("Plain"); /*Default white background for all plots*/
  
   /* set bkg color of all to 10: white, but not 0*/
   gStyle->SetCanvasColor(kWhite);
   gStyle->SetFrameFillColor(kWhite);
   gStyle->SetStatColor(kWhite);
   gStyle->SetPadColor(kWhite);
   gStyle->SetFillColor(10);
   gStyle->SetTitleFillColor(kWhite);

   /* SetPaperSize wants width & height in cm: A4 is 20,26 & US is 20,24*/
   gStyle->SetPaperSize(20, 26); 
   /* No yellow border around histogram*/
   gStyle->SetDrawBorder(0);
   /* remove border of canvas*/
   gStyle->SetCanvasBorderMode(0);
   /* remove border of pads*/
   gStyle->SetPadBorderMode(0);
   gStyle->SetFrameBorderMode(0);
   gStyle->SetLegendBorderSize(0);
  
   /* default text size*/
   gStyle->SetTextSize(0.05);
   gStyle->SetTitleSize(0.07,"xyz");
   gStyle->SetLabelSize(0.06,"xyz");

   gStyle->SetTitleSize(0.07,"other");
   gStyle->SetLabelSize(0.06,"other");

   /* title offset: distance between given text and axis, here x,y,z*/
   gStyle->SetLabelOffset(0.015,"xyz");
   gStyle->SetTitleOffset(1.1,"yz");
   gStyle->SetTitleOffset(1.0,"x");

   gStyle->SetLabelOffset(0.015,"other");
   gStyle->SetTitleOffset(1.1,"other");


   /* Use visible font for all text*/
   int font = 42; 
   gStyle->SetTitleFont(font);
   gStyle->SetTitleFontSize(0.06);
   gStyle->SetStatFont(font);
   gStyle->SetStatFontSize(0.07);
   gStyle->SetTextFont(font);
   gStyle->SetLabelFont(font,"xyz");
   gStyle->SetTitleFont(font,"xyz");

   gStyle->SetLabelFont(font,"other");
   gStyle->SetTitleFont(font,"other");

   gStyle->SetTitleBorderSize(0);
   gStyle->SetStatBorderSize(1);

   /* big marker points*/
   gStyle->SetMarkerStyle(1);
   gStyle->SetLineWidth(2);  
   gStyle->SetMarkerSize(1.2);
   /*set palette in 2d histogram to nice and colorful one*/
   gStyle->SetPalette(1,0); 

   /*No title for histograms*/
   gStyle->SetOptTitle(0);
   /* show the errors on the stat box */
   gStyle->SetOptStat(0); 
   /* show errors on fitted parameters*/
   gStyle->SetOptFit(0); 
   /* number of decimals used for errors*/
   gStyle->SetEndErrorSize(5);   

   /* set line width to 2 by default so that histograms are visible when printed small
      idea: emphasize the data, not the frame around*/
   gStyle->SetHistLineWidth(2);
   gStyle->SetFrameLineWidth(2);
   gStyle->SetFuncWidth(2);
   gStyle->SetHistLineColor(kBlack);
   gStyle->SetFuncColor(kRed);
   gStyle->SetLabelColor(kBlack,"xyz");

   gStyle->SetLabelColor(kBlack,"other");

   //set the margins
   gStyle->SetPadBottomMargin(0.18);
   gStyle->SetPadTopMargin(0.08);
   gStyle->SetPadRightMargin(0.08);
   gStyle->SetPadLeftMargin(0.17);
   
   //set the number of divisions to show
   gStyle->SetNdivisions(506, "xy");

   gStyle->SetNdivisions(506, "other");
   
   //turn off xy grids
   gStyle->SetPadGridX(0);
   gStyle->SetPadGridY(0);
   
   //set the tick mark style
   gStyle->SetPadTickX(1);
   gStyle->SetPadTickY(1);

   gStyle->SetCanvasDefW(800);
   gStyle->SetCanvasDefH(700);

   gROOT->ForceStyle();
}


  /// print cumulative fraction for given points for given histogram
  void PrintCumulativeFractionAt( TH1* h, const std::vector<double>& points, bool forward ) {

    TH1* cumulative = h->GetCumulative( forward );
    if ( forward ) {
      cumulative->Scale( 1.0 / cumulative->GetBinContent( cumulative->FindLastBinAbove(0) ) );
    } else {
      cumulative->Scale( 1.0 / cumulative->GetBinContent( cumulative->FindFirstBinAbove(0) ) );
    }

    for (auto const& point : points ) {
      std::cout << std::setw(7)  << point
		<< std::setw(15) << cumulative->GetBinContent( cumulative->FindFixBin( point ) )
		<< std::endl;
    }
    delete cumulative;
  }

  TLatex* CLICdpLabel(TCanvas& canv, std::string const& status, Double_t x, Double_t y, Color_t color){

    canv.cd();
    TLatex* l = new TLatex();
    l->SetNDC();
    l->SetTextFont(42);
    l->SetTextColor(color);
    l->SetTextSize(0.045);
    std::string label("CLICdp ");
    l->DrawLatex(x,y,label.c_str());
    TLatex l2;
    l2.SetNDC();
    l2.SetTextFont(42);
    l2.SetTextColor(color);
    l2.SetTextSize(0.035);
    //double dx = l2.GetTextSize()+0.005;
    double dx = 0.4*l->GetTextSize()*label.length();
    l2.DrawLatex(x+dx,y,status.c_str());
    return l;

  }//end CLICdpLabel


}//namespace


  // void SetCDRStyle2(Int_t DefaultFont){
  //   _DefaultFont = DefaultFont;
  //   gROOT->SetStyle("Plain");// Default white background for all plots
  //   // set bkg color of all to 10: white, but not 0
  //   gStyle->SetCanvasColor(10);
  //   gStyle->SetFrameFillColor(10);
  //   gStyle->SetStatColor(10);
  //   gStyle->SetPadColor(10);
  //   gStyle->SetFillColor(10);
  //   // SetPaperSize wants width & height in cm: A4 is 20,26 & US is 20,24
  //   gStyle->SetPaperSize(20,26);
  //   // No yellow border around histogram
  //   gStyle->SetDrawBorder(0);
  //   // removve border of canvas
  //   gStyle->SetCanvasBorderMode(0);
  //   // remove border of pads
  //   gStyle->SetPadBorderMode(0);
  //   // default text size
  //   gStyle->SetTextSize(0.05);
  //   gStyle->SetTitleSize(0.07,"xyz");
  //   gStyle->SetLabelSize(0.06,"xyz");//AM From 0.04
  //   // title offset: distance between given text and axis, here x,y,z
  //   gStyle->SetLabelOffset(0.01,"x");//AS Customized //Default 0.01
  //   gStyle->SetLabelOffset(0.01,"yz");//AS Customized
  //   gStyle->SetLabelOffset(0.005,"y");//AS Customized
  //   gStyle->SetTitleOffset(1.1,"yz");//AM From 0.9
  //   gStyle->SetTitleOffset(1.0,"x");//AM From 0.7
  //   // Use visible font for all text
  //   gStyle->SetTitleFont(DefaultFont);
  //   gStyle->SetTitleFontSize(0.06);
  //   gStyle->SetStatFont(DefaultFont);
  //   gStyle->SetTextFont(DefaultFont);
  //   gStyle->SetLabelFont(DefaultFont,"xyz");
  //   gStyle->SetTitleFont(DefaultFont,"xyz");
  //   // big marker points
  //   gStyle->SetMarkerSize(1.2);
  //   //set palette in 2d histogram to nice and colorful one
  //   gStyle->SetPalette(1,0);
  //   //No Title for histograms
  //   gStyle->SetOptTitle(0);
  //   // show the errors on the stat box
  //   gStyle->SetOptStat(0); //AS CHANGED
  //   // show errors on fitted parameters
  //   gStyle->SetOptFit(0); //AS CHANGED
  //   // number of decimals used for errors
  //   gStyle->SetEndErrorSize(5);

  //   // set line width to 2 by default so that histograms are visible when printed small
  //   // idea: emphasize the data, not the frame around
  //   gStyle->SetHistLineWidth(2);// is much better visible for multiple lines and faint colors!, Changed from my 3

  //   //AS Customized
  //   gStyle->SetLabelOffset(0.01,"other");//AS Customized
  //   gStyle->SetTitleOffset(1.9,"other");
  //   gStyle->SetLabelFont(DefaultFont,"other");
  //   gStyle->SetTitleFont(DefaultFont,"other");

  //   // //These are the Default canvas sizes
  //   // gStyle->SetCanvasDefW(700);//AM from 800
  //   // gStyle->SetCanvasDefH(500);//AM from 700

  //   //These are the Default canvas sizes
  //   gStyle->SetCanvasDefW(800);
  //   gStyle->SetCanvasDefH(700);



  //   //AM additions
  //   gStyle->SetTitleFillColor(0);
  //   gStyle->SetTitleBorderSize(0);
  //   gStyle->SetStatBorderSize(1);
  //   gStyle->SetFrameBorderMode(0);

  //   gStyle->SetLegendBorderSize(1);


  //   //    gStyle->SetMarkerStyle(20);///AS
  //   gStyle->SetLineWidth(2);

  //   gStyle->SetStatFontSize(0.07);



  //   gStyle->SetFrameLineWidth(2);
  //   gStyle->SetFuncWidth(2);
  //   gStyle->SetHistLineColor(kBlack);
  //   gStyle->SetFuncColor(kRed);
  //   gStyle->SetLabelColor(kBlack,"xyz");


  //   //set the margins
  //   gStyle->SetPadBottomMargin(0.18);
  //   gStyle->SetPadTopMargin(0.08);
  //   gStyle->SetPadRightMargin(0.08);
  //   gStyle->SetPadLeftMargin(0.17);

  //   //set the number of divisions to show
  //   gStyle->SetNdivisions(506, "xy");

  //   //turn off xy grids
  //   gStyle->SetPadGridX(0);
  //   gStyle->SetPadGridY(0);

  //   //set the tick mark style
  //   gStyle->SetPadTickX(0);
  //   gStyle->SetPadTickY(1);

  //   gROOT->ForceStyle();
  // }
