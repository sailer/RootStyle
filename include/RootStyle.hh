#ifndef RootStyle_hh
#define RootStyle_hh 1

#include <TROOT.h>
#include <TMath.h>

#include <iomanip>
#include <iostream>


//Forward Declaration of RootClasses
class TLegend;
class TProfile;
class TPaletteAxis;
class TPaveText;
class TGraph;
class TGraphErrors;
class TH1; class TH1D; class TH2;
class THStack;
class TCanvas;
class TAxis;
class TPad;
class TEfficiency;
class TPave;
class TLatex;

namespace RootStyle{

class PDFFile;
class PSFile;
class MyColor;
class MyMarker;


  // static Int_t DefaultFont=133;
  // void SetDefaultFont(Int_t NewFont) { DefaultFont = NewFont; }
  // //  DefaultFont = 133; //This font is not sized relatively to the canvas size!

  class MyColor {
  private:
    std::vector<Color_t> *Colors;
  public:
    MyColor();
    ~MyColor();
    Color_t GetColor();
    void SetColors();
  };//Class MyColor

  class MyMarker {
  private:
    std::vector<Marker_t> *Markers;
  public:
    MyMarker();
    ~MyMarker();
    Marker_t GetMarker();
    void SetMarkers();
  };//Class MyMarker
  
  void SetStyle(Int_t DefaultFont = 132);
  void SetBeamerStyle(Int_t DefaultFont = 42);
  void SetCDRStyle(Int_t DefaultFont = 42);
  void SetCDRStyle2(Int_t DefaultFont = 42);
  void SetCDRStyleReal();

  void SetPad(TCanvas *c2);
  void SetPad1D(TCanvas *c2);
  void SetPad2D(TPad *pad);
  void SetPad2D(TCanvas *c2);
  void BinLogX(TH1*h, TString Saxis);
  void NormalizeLogHisto(TH1 *h);
  void NormalizeLogHisto(TH2 *h);


  TLegend* BuildLegend(TCanvas& canvas, Double_t x1, Double_t y1, Double_t sizePerEntry=0.045,
                       std::vector<std::string> const& header={}, int at=0);

  TLegend* BuildLegend(TCanvas& canvas, Double_t x1, Double_t y1, Double_t x2, Double_t y2);
  TLegend* BuildLegend(TCanvas &canvas, Double_t x1, Double_t y1, Double_t x2, Double_t y2, TString& header);
  TLegend* BuildLegend(TCanvas *canvas, Double_t x1, Double_t y1, Double_t x2, Double_t y2);
  TLegend* BuildLegend(TCanvas *canvas, Double_t x1, Double_t y1, Double_t x2, Double_t y2, TString& Header);
  TLegend* Build2DLegend(TCanvas *canvas);
  void AddLegendHeader(TLegend* leg, std::vector<std::string> const& lines, int at=0);
  TPaveText* CreateTextBox(Double_t x1, Double_t y1, std::vector<std::string> const& lines);

  inline double GetLegendEntrySize() { return 0.0625; }
  inline double GetLegendTextSize() { return 0.06; }

  void RightSizeLegend( TLegend* leg, double sizePerLine = 0.0625 );
  void SetPaveCoordinates(TPave *pave, double x1, double y1, double x2, double y2);

  inline void SetLegendCoordinates(TLegend *leg, double x1, double y1, double x2, double y2){
    SetPaveCoordinates((TPave*)leg, x1, y2, x2, y1);
  }

  TLatex* CLICdpLabel(TCanvas& canv,
		      std::string const& status="work in progress",
		      Double_t x=0.20,
		      Double_t y=0.86,
		      Color_t color=kBlack);


  inline TLatex* CLICdpLabelTopLeft(TCanvas& canv, std::string const& status) {
    return CLICdpLabel(canv, status, 0.20, 0.86);
  }

  inline TLatex* CLICdpLabelTopRight(TCanvas& canv, std::string const& status) {
    return CLICdpLabel(canv, status, 0.77, 0.86);
  }

  inline TLatex* CLICdpLabelBottomLeft(TCanvas& canv, std::string const& status) {
    return CLICdpLabel(canv, status, 0.20, 0.22);
  }

  inline TLatex* CLICdpLabelBottomRight(TCanvas& canv, std::string const& status) {
    return CLICdpLabel(canv, status, 0.76, 0.22);
  }

  inline TLatex* CLICdpLabelAbove(TCanvas& canv, std::string const& status) {
    return CLICdpLabel(canv, status, 0.20, 0.93);
  }


  TProfile* AverageHistograms(std::vector<TH1D*> histoVector);
  void PrintCumulativeFractionAt( TH1* h, const std::vector<double>& points, bool forward=true );

  void SetAxisOfTEfficiency(TEfficiency &tEff, std::string const& TitleX, std::string const& TitleY);
  void AutoScaleXaxis( THStack *s );
  void AutoScaleXaxis( TH1 *hist );
  double AutoSetYRange( TCanvas& canv, double maxScale=1.1);
  double EqualiseHistogramPeak( TCanvas& canv );

  void SetAllColors( TH1* object, Color_t color );
  void SetAllColors( TEfficiency* object, Color_t color );
  void SetAllColors( TGraph* object, Color_t color );
  void SetAllColors( TGraphErrors* object, Color_t color );

  TPaletteAxis* MovePaletteHorizontally(TH1 *histo, Double_t step=0.05);

  void SaveCanvas(TCanvas *canvas, TString filename);

  void SetColors();
  Color_t GetColor();
  void SetMarkers();
  Marker_t GetMarker();


  template <class T> Double_t averageArray(T *array, Int_t size){
    Double_t mean = 0;
    for(Int_t i = 0; i  < size; ++i) {
      mean += double(array[i]);
    }
    mean /= Double_t(size);
    return mean;
  }

  template <class T> Double_t varianceArray(T *array, Int_t size){
    std::cerr << "WARNING:  Used variance, was wrong before"  << std::endl;
    Double_t mean = averageArray(array, size);
    Double_t variance = 0;
    for(Int_t i = 0; i  < size; ++i) {
      variance += (double(array[i])-mean)*(double(array[i])-mean);
    }
    variance = TMath::Sqrt(variance/double(size));
    return variance;
  }

  template <class T> Double_t varianceVector(T *array, Int_t size){
    Double_t mean = averageArray(array, size);
    Double_t variance = 0;
    for(Int_t i = 0; i  < size; ++i) {
      variance += (double(array[i])-mean)*(double(array[i])-mean);
    }
    variance = TMath::Sqrt(variance/double(size));
    return variance;
  }

  template <class T> Double_t rmsArray(T *array, Int_t size){
    Double_t rms = 0;
    for(Int_t i = 0; i  < size; ++i) {
      rms += ( double(array[i]) ) * ( double(array[i]) );
    }
    rms = TMath::Sqrt(rms/double(size));
    return rms;
  }


  // Double_t averageArray(Double_t *array, Int_t size);
  // Double_t varianceArray(Double_t *array, Int_t size);

  class PDFFile {
  public:
    PDFFile();
    PDFFile(TString filename);
    ~PDFFile();
    void AddCanvas (TCanvas *canvas, Option_t* title="");
    void AddCanvas (TCanvas &canvas, Option_t* title="") { AddCanvas(&canvas, title); }
    void CloseFile();
  private:
    Bool_t fileOpened;
    TString fileName;
    static int fileNumber;
  };//class


  class PSFile {
  public:
    PSFile();
    PSFile(TString filename);
    ~PSFile();
    void AddCanvas (TCanvas *canvas);
    void CloseFile();
  private:
    Bool_t fileOpened;
    TString fileName;
    static int fileNumber;
  };//class

  template <typename t>
  inline void PrintTreeProgress(t const& entry, t const& entries) {
    if(entry%(entries/100)==0) {
      std::cout << '\r' << "Running: " << std::setw(10) <<int(100*(double(entry)/entries)) << "%" << std::flush;
    }
  }//PrintTreeProgess

  template <typename t>
  inline void PrintTreeProgess(t const& entry, t const& entries) {
    PrintTreeProgress(entry, entries);
  }

  inline void PrintTreeProgess(int const& entry, int const& entries) {
    PrintTreeProgress(entry, entries);
  }


}//namespace

#endif


