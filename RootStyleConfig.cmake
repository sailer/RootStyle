###############################################
# cmake configuration file for RootStyle
# @author Andre Sailer, CERN
###############################################

SET( RootStyle_FOUND FALSE )
MARK_AS_ADVANCED( RootStyle_FOUND )

# do not store find results in cache
SET( RootStyle_INCLUDE_DIR RootStyle_INCLUDE_DIR-NOTFOUND )

FIND_PATH( RootStyle_INCLUDE_DIR
	NAMES RootStyle.hh
	PATHS ${RootStyle_DIR}
	PATH_SUFFIXES include
	NO_DEFAULT_PATH
)
IF( NOT RootStyle_INCLUDE_DIR )
    MESSAGE( STATUS "Check for RootStyle: ${RootStyle_DIR}"
					" -- failed to find RootStyle include directory!!" )
ELSE( NOT RootStyle_INCLUDE_DIR )
    MARK_AS_ADVANCED( RootStyle_INCLUDE_DIR )
ENDIF( NOT RootStyle_INCLUDE_DIR )


# do not store find results in cache
SET( RootStyle_LIB RootStyle_LIB-NOTFOUND )

FIND_LIBRARY( RootStyle_LIB
	NAMES RootStyle
	PATHS ${RootStyle_DIR}
	PATH_SUFFIXES lib
	NO_DEFAULT_PATH
)

IF( NOT RootStyle_LIB )
    MESSAGE( STATUS "Check for RootStyle: ${RootStyle_DIR}"
					" -- failed to find RootStyle library!!" )
ENDIF( NOT RootStyle_LIB )


# set variables and display results
IF( RootStyle_INCLUDE_DIR AND RootStyle_LIB )
    SET( RootStyle_FOUND TRUE )
    SET( RootStyle_INCLUDE_DIRS ${RootStyle_INCLUDE_DIR} )
	SET( RootStyle_LIBRARIES ${RootStyle_LIB} )
	MARK_AS_ADVANCED( RootStyle_INCLUDE_DIRS RootStyle_LIBRARIES )
	MESSAGE( STATUS "Check for RootStyle: ${RootStyle_DIR} -- works" )
ELSE( RootStyle_INCLUDE_DIR AND RootStyle_LIB )
	IF( RootStyle_FIND_REQUIRED )
		MESSAGE( FATAL_ERROR "Check for RootStyle: ${RootStyle_DIR} -- failed!!" )
    ELSE( RootStyle_FIND_REQUIRED )
        MESSAGE( STATUS "Check for RootStyle: ${RootStyle_DIR}"
						" -- failed!! will skip this package..." )
    ENDIF( RootStyle_FIND_REQUIRED )
ENDIF( RootStyle_INCLUDE_DIR AND RootStyle_LIB )
